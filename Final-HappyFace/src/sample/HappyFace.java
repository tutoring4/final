package sample;

import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

class HappyFace extends StackPane {
  public HappyFace(double radius) {
    // pane
    Pane pane = new Pane();

    // circle
    Circle head = new Circle(radius, radius, radius);
    head.setFill(Color.WHITE);
    head.setStroke(Color.BLACK);

    //mouth
    Arc mouth = new Arc(radius, radius + radius / 2.7, radius / 2, radius / 5, 180, 180);
    mouth.setType(ArcType.OPEN);
    mouth.setFill(Color.WHITE);
    mouth.setStroke(Color.BLACK);

    // vars
    public double factor = 5;
    public double topX = radius;
    public double topY = radius - radius / factor;
    public double leftX = topY;
    public double leftY = radius + radius / factor;
    public double rightX = leftY;
    public double rightY = leftY;
    Polygon nose = new Polygon(topX, topY, leftX, leftY, rightX, rightY);
    nose.setFill(Color.WHITE);
    nose.setStroke(Color.BLACK);

    // ellipse
    Ellipse left = new Ellipse(radius - radius / 3, radius - radius / 3, radius / 5, radius / 8);
    left.setFill(Color.WHITE);
    left.setStroke(Color.BLACK);
    Ellipse right = new Ellipse(radius + radius / 3, radius - radius / 3, radius / 5, radius / 8);
    right.setFill(Color.WHITE);
    right.setStroke(Color.BLACK);

    // eye
    Circle leftEyePupil = new Circle(left.getCenterX(), left.getCenterY(),
      left.getRadiusX() / 2);
    Circle rightEyePupil = new Circle(right.getCenterX(), right.getCenterY(),
      right.getRadiusX() / 2);

    // assign peices together
    pane.getChildren().addAll(head, mouth, nose, left, leftEyePupil, right, rightEyePupil);
    getChildren().add(pane);
  }
}
