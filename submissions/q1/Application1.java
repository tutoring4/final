package sample;
// Q1
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Application1 extends Application {

    @Override
    public void start(Stage primaryStage) {
        HappyFace pane = new HappyFace(200);
        pane.setPadding(new Insets(20));

        Scene scene = new Scene(pane);
        primaryStage.setTitle("java2s.com");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}