package src.com.hungrybird;
// Q2
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

class MovingFan extends JPanel {
	// config for fan
	int x, y, angel;
	int fanWidth = 500;
	int fanHeight = 500;
	int centerX, centerY;
	int mode;

	MovingFan(int w, int h) {
		this.centerX = w / 2;
		this.centerY = h / 2;
		this.x = centerX - fanWidth / 2;
		this.y = centerY - fanHeight / 2;
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		g.setColor(Color.blue);
		g.fillArc(x, y, fanWidth, fanHeight, angel, 30);
		g.fillArc(x, y, fanWidth, fanHeight, angel + 90, 30);
		g.fillArc(x, y, fanWidth, fanHeight, angel + 180, 30);
		g.fillArc(x, y, fanWidth, fanHeight, angel + 270, 30);

		// mode -> 1, clockwise, mode -> 2, counter clockwise
		if(mode == 1) {
			angel = (angel - 90) % 260;
		} else if(mode == 2) {
			angel = (angel + 90) % 260;
		}
		
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		repaint();
	}

	public void switchMode(int mode) {
		this.mode = mode;
	}
}
