package org.hungrybird.exception;
// Q3
import java.util.Scanner;

public class InputException {
  private Scanner sc = new Scanner(System.in);

  // constructor
  public InputException() {
    String str = "Please enter two integers: ";
    System.out.print(str);
    String line = sc.nextLine();
    String[] inputs = line.split(" ");
    boolean isNumeric = isNumber(inputs);

    while (!isNumeric) {
      System.out.println("You must enter integer numbers.");
      System.out.print(str);

      line = sc.nextLine();
      inputs = line.split(" ");
      isNumeric = isNumber(inputs);
    }

    int input1 = Integer.parseInt(inputs[0]);
    int input2 = Integer.parseInt(inputs[1]);
    System.out.printf("%d + %d = %d\n", input1, input2, input1 + input2);
  }

  // check the number is number
  private boolean isNumber(String[] inputs) {
    boolean isValid1 = inputs[0].matches("[-+]?\\d*\\.?\\d+");
    boolean isValid2 = inputs[1].matches("[-+]?\\d*\\.?\\d+");
    return isValid1 && isValid2;
  }
}
