package org.hungrybird.exception;
// Q3
import java.util.Random;
import java.util.Scanner;

public class ArrayException {
  private int[] nums = new int[100];
  private Scanner sc = new Scanner(System.in);

  public ArrayException() {
    this.seeding();
    this.prompt();
  }

  // random numbers to array
  private void seeding() {
    Random rand = new Random();
    for (int i = 0; i < 100; i++) {
      this.nums[i] = rand.nextInt(50);
    }
  }

  // prompt user
  private void prompt() {
    System.out.print("Please enter the index: ");
    String index = this.sc.nextLine();

    while (!this.validate(index)) {
      System.out.print("Please enter the index: ");
      index = this.sc.nextLine();
    }

    int idx = Integer.parseInt(index);
    System.out.printf("array[%d] = %d", idx, this.nums[idx]);
  }

  // validate the user inputs
  private boolean validate(String index) {

    boolean isNum = index.matches("[-+]?\\d*\\.?\\d+");
    if (!isNum) {
      System.out.println("Not-a-Number (NaN)");
      return false;
    }
    int idx = Integer.parseInt(index);
    if (idx < 0 || idx >= 100) {
      System.out.println("Out of bounds");
      return false;
    }
    return true;
  }
}