package src.com.hungrybird;
// Q2
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Application extends JFrame implements ActionListener {

	JButton button1, button2;
	MovingFan fan;

	// starts everything
	public Application() {
		// base config
		setSize(1000, 1000);
		setTitle("Moving Fan");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setLayout(new GridLayout(2, 4));

		JFrame frame = new JFrame();
		
		button1 = new JButton("Rotate Clockwise");
		button1.addActionListener(this);

		button2 = new JButton("Rotate Coutner Clockwise");
		button2.addActionListener(this);

		// panel for moving fan
		fan = new MovingFan(780, 560);
		fan.setLayout(new GridLayout(2, 1));
		fan.setBackground(Color.white);
		fan.setSize(800, 800);

		// panel for buttons
		JPanel bottom = new JPanel();
		bottom.setLayout(new GridLayout(1, 1));
		bottom.add(button1);
		bottom.add(button2);

		// add those panels to main screen
		add(fan, BorderLayout.CENTER);
		add(bottom, BorderLayout.SOUTH);
	}

	public static void main(String[] args) {
		new Application().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.button1) {
			fan.switchMode(1);
		} else if (e.getSource() == this.button2) {
			fan.switchMode(2);
		}
	}
}